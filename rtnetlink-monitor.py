#!/usr/bin/env python3

from pyroute2 import IPDB
import pprint
import logging
import sys
from ipaddress import ip_address, ip_network
import os
import yaml

# Configuration
CONFIG = os.getenv("RTNETLINK_MONITOR_CONF", "/etc/rtnetlink-monitor.yml")
try:
    with open(CONFIG, 'r') as f:
        config = yaml.safe_load(f)
        events, tables, protos, destinations = config.values()
except FileNotFoundError:
    sys.exit("Configuration file not found")

# Logging
log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler(sys.stdout))
log.setLevel(logging.INFO)
pp = pprint.PrettyPrinter(indent=3).pprint


def process_rtmsg(ipdb, msg):
    event = msg.get('event').strip('RTM_')
    dst = msg.get_attr('RTA_DST')
    mask = msg.get('dst_len')
    proto = protos[str(msg.get('proto'))]
    table = tables[str(msg.get('table'))]
    multipath = msg.get_attr('RTA_MULTIPATH')
    if multipath is not None:
        path = _get_paths(multipath, ipdb)
    else:
        oif = msg.get_attr('RTA_OIF')
        iface = ipdb.by_index[oif].ifname
        gw = msg.get_attr('RTA_GATEWAY', 'direct')
        path = 'via {} dev {}'.format(gw, iface)
    log.info('{} to {}/{} {} proto {} table {}'.format(event, dst, mask, path, proto, table))
    # pp(msg)


def _get_paths(multipath, ipdb):
    paths = list()
    for path in multipath:
        oif = path['oif']
        _, gw = path['attrs'][0]
        iface = ipdb.by_index[oif].ifname
        paths.append('nexthop {}:{}'.format(gw, iface))
    return ' '.join(paths)


def interested_in(msg):
    # Check it's a routing event with destination set
    if (msg.get('event', None) not in events or
            msg.get_attr('RTA_DST', None) is None):
        return False

    # Check that routing table is of interest
    table = str(msg.get_attr('RTA_TABLE', None))
    if table not in tables.keys():
        return False

    # Check that destination IP is of interest
    try:
        dst = msg.get_attr('RTA_DST', None)
        match = list(
                map(lambda network: ip_address(dst) in ip_network(network),
                    destinations))
        if any(match):
            return True
    except ValueError:
        return False
    return False


with IPDB() as ipdb, ipdb.eventqueue() as evq:
    for msg in evq:
        if interested_in(msg):
            process_rtmsg(ipdb, msg)
