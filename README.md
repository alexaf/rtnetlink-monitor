# rtnetlink-monitor

rtnetlink-monitor is a simple tool leveraging `pyroute2` to parse messages from
the rtnetlink socket of the Linux kernel. It offers visibility over a host's
routing state changes.

rtnetlink-monitor can filters messages based on specific selectors, namely:

- rtnetlink message types (or events), e.g. route addition.
- routing tables, e.g. the main routing table
- layer 3 destinations, i.e. IP subnets we're insterested in

For every message matching the aforementioned selectors a informational log is
printed to stdout.

Its functionality resembles `ip monitor route` (offered by `iproute2`) but allows
us to filter and process messages in a more flexible way. Additionally it can be
run as systemd service.

## How to use it

Installing the debian package built by Gitlab CI is probably the easiest way to
get rtnetlink-monitor up an running. The debian package will handle the runtime
dependencies, offer a systemd service as well as a default configuration.

You may fetch the CI built debian package from the latest [release](https://gitlab.com/alexaf/rtnetlink-monitor/-/releases)

Or you may build it yourself (assuming you already have git-buildpackage) like:

```
sudo gbp buildpackage -uc -us --git-ignore-branch --git-upstream-tree=SLOPPY --git-export-dir=./build
```

### Configuration

Configuration is pretty simple; you only have to specify the selectors based
on which rtnetlink events will be logged.

`rtnetlink-monitor.yml` serves as an example configuration file. One may
check rtnetlink(7) for details regarding the rtnetlink messages structures,
event types etc.

### Demo output

As seen with:

```
sudo journalctl -u rtnetlink-monitor -f
```

Manually added routed for a single IP, directly connected through `wlp2s0` interface:

```
Sep 20 21:29:25 russell rtnetlink-monitor[4718]: NEWROUTE to 10.1.2.3/32 via direct dev wlp2s0 proto boot table main
```

Multipath route addition and deletion for a subnet, routes handled by `bird` routing daemon:
```
Sep 20 21:36:10 russell rtnetlink-monitor[4718]: NEWROUTE to 10.0.9.64/28 nexthop 192.168.1.250:eth0 nexthop 192.168.1.249:wlp2s0 proto bird table main
Sep 20 21:37:33 russell rtnetlink-monitor[4718]: DELROUTE to 10.0.9.64/28 nexthop 192.168.1.250:eth0 nexthop 192.168.1.249:wlp2s0 proto bird table main

```

Route deletion for a single IP address via gateway, by `bird` on table `public`:

```
Sep 20 22:32:55 russell rtnetlink-monitor[5105]: DELROUTE to 1.1.1.1/32 via 192.168.1.1 dev wlp2s0 proto bird table main
```

### Manual invocation

Although the suggested way of running rtnetlink-monitor is as a systemd service
one may always run it manually like:

` ~ RTNETLINK_MONITOR_CONF=my_conf.yml ./rtnetlink-monitor.py`

### Compatibility

- rtnetlink-monitor requires at least pyroute2 `0.5.1` as it makes use of pyroute2 eventqueue.
  This means Debian Buster or Ubuntu Focal, and later.
